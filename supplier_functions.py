from web3 import Web3, HTTPProvider # web3.py docs
import os # python docs
import json # python docs

with open('oem_supplier_shipper.json') as json_file: 
    data = json.load(json_file)

abi = data['abi']

contract_address = "0x7C1c21995af5E2920edf30D662C3fDa103423cC3"

from_address = "0x7bDB60C47b0CD90179014b4e12D5cA89780bF3d6"
private_key = '4aac226205502d8a9802c58ba16363cbe0787a8c6c1e8ffb5d9f8efbfc5e336f'


w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs
w3.eth.defaultAccount = from_address


Contract = w3.eth.contract(abi = abi, address = contract_address)

nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

insurance = Contract.functions.insurance().call()

txn = Contract.functions.payment_from_supplier().buildTransaction({
    'gas' : 70000,
    'gasPrice' : w3.toWei('1', 'gwei'),
    'nonce' : nonce,
    'value' : insurance,
})

signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

print(w3.eth.waitForTransactionReceipt(signed_txn_hash))