# from web3 import Web3, HTTPProvider # web3.py docs
# import os # python docs
# import json # python docs

# with open('oem_supplier_shipper.json') as json_file: 
#     data = json.load(json_file)

# abi = data['abi']

# contract_address = "0x7C1c21995af5E2920edf30D662C3fDa103423cC3"

# from_address = "0x65a705ba33CeF50E52AbFE5cE833c922d989a3B8"
# private_key = '91280a5b1a1c34ee22af4333688c8ed3a89027394e80e742a69928f28f21c2a9'


# w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs
# w3.eth.defaultAccount = from_address


# Contract = w3.eth.contract(abi = abi, address = contract_address)

# nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

# price = Contract.functions.price().call()
# bonus = Contract.functions.bonus().call()

# txn = Contract.functions.payment_from_oem().buildTransaction({
#     'gas' : 70000,
#     'gasPrice' : w3.toWei('1', 'gwei'),
#     'nonce' : nonce,
#     'value' : price + bonus,
# })

# signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

# signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

# w3.eth.waitForTransactionReceipt(signed_txn_hash)


# with functions

from web3 import Web3, HTTPProvider # web3.py docs
import os # python docs
import json # python docs

with open('oem_supplier_shipper.json') as json_file: 
    data = json.load(json_file)

abi = data['abi']

contract_address = "0x7C1c21995af5E2920edf30D662C3fDa103423cC3"

from_address = "0x65a705ba33CeF50E52AbFE5cE833c922d989a3B8"
private_key = '91280a5b1a1c34ee22af4333688c8ed3a89027394e80e742a69928f28f21c2a9'


w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs
w3.eth.defaultAccount = from_address


Contract = w3.eth.contract(abi = abi, address = contract_address)

nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

def payment(Contract = Contract, nonce = nonce, w3 = w3):

    price = Contract.functions.price().call()
    bonus = Contract.functions.bonus().call()

    txn = Contract.functions.payment_from_oem().buildTransaction({
        'gas' : 70000,
        'gasPrice' : w3.toWei('1', 'gwei'),
        'nonce' : nonce,
        'value' : price + bonus,
    })

    signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

    signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    w3.eth.waitForTransactionReceipt(signed_txn_hash)

def qc(passed, Contract = Contract, nonce = nonce, w3 = w3):

    txn = Contract.functions.quality_check(passed).buildTransaction({
        'gas' : 1000000,
        'gasPrice' : w3.toWei('1', 'gwei'),
        'nonce' : nonce,
    })

    signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

    signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print(w3.eth.waitForTransactionReceipt(signed_txn_hash))


def no_accepted_delivery_time(Contract = Contract, nonce = nonce, w3 = w3):

    txn = Contract.functions.no_accepted_delivery_time().buildTransaction({
        'gas' : 70000,
        'gasPrice' : w3.toWei('1', 'gwei'),
        'nonce' : nonce
    })

    signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

    signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
