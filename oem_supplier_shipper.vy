Payment : event ({_from:indexed(address), _to:indexed(address), _value: wei_value})
Shipment : event ({_from:indexed(address), _contract_address:indexed(address), _status: int128})

# 0x65a705ba33CeF50E52AbFE5cE833c922d989a3B8
oem: public(address)
# 0x7bDB60C47b0CD90179014b4e12D5cA89780bF3d6
supplier: public(address)
# 0x5E536f1A1b0A21F1044244F02ABE2902576462b7
oracle: public(address)

delivered: public(bool)
has_oem_payed: public(bool)
has_supplier_payed: public(bool)
has_shipment_started: public(bool)
has_quality_control_being_done : public(bool)

price: public(wei_value)
malus: public(wei_value)
bonus: public(wei_value)
insurance: public(wei_value)

production_duration_limit: public(timedelta)
shipment_duration_limit: public(timedelta)
shipment_duration_inacceptable: public(timedelta)


orderStart: public(timestamp)
shipmentStart: public(timestamp)
shipmentEnd: public(timestamp)



@public
def __init__(_supplier : address, _price : wei_value, _malus : wei_value
            , _bonus : wei_value, _insurance : wei_value, _production_duration_limit: timedelta
            , _shipment_duration_limit: timedelta, _shipment_duration_inacceptable: timedelta):
    self.oem = 0x65a705ba33CeF50E52AbFE5cE833c922d989a3B8
    self.supplier = _supplier
    self.oracle = 0x5E536f1A1b0A21F1044244F02ABE2902576462b7
    self.delivered = False
    self.has_oem_payed = False
    self.has_supplier_payed = False
    self.has_shipment_started = False
    self.has_quality_control_being_done = False
    self.price = _price
    self.malus = _malus
    self.bonus = _bonus
    self.insurance = _insurance
    self.production_duration_limit = _production_duration_limit
    self.shipment_duration_limit = _shipment_duration_limit
    self.shipment_duration_inacceptable = _shipment_duration_inacceptable


@public
def get_contract_balance() -> wei_value:
    return self.balance

@public
@payable
def payment_from_oem():
    assert msg.sender == self.oem # assures that the sender of the payment is the oem
    assert msg.value >= self.price + self.bonus # assures that the correct amount has been paid
    assert self.has_oem_payed == False # assures that the oem has not payed yet in order to avoid 
                                       # that the oem send more than one time the payment in the contract

    self.has_oem_payed = True

    self.orderStart = block.timestamp # the official start of the order will be the point of time
                                      # where the oem has send the payment to the contract
    log.Payment(msg.sender, self, msg.value) # sends a message through the blockchain 
                                             # that oem has sent the payment to the smart contract

@public
@payable
def payment_from_supplier():
    assert msg.sender == self.supplier 
    assert msg.value >= self.insurance
    assert self.has_oem_payed == True # assures that the oem has already made the payment in the contract
    assert self.has_supplier_payed == False # assures that the supplier has not payed yet the insurance in the contract
                                            # in order to avoid that the supplier send it more than once 

    self.has_supplier_payed = True
    log.Payment(msg.sender, self, msg.value) # sends a message through the blockchain
                                             # that the supplier has sent the inssurance to the smart contract

@public
def shipping_start():
    """
    """
    assert msg.sender == self.oracle 
    assert self.has_oem_payed == True
    assert self.has_supplier_payed == True
    assert self.has_shipment_started == False # assures that the shipment of the order has not started yet 


    self.has_shipment_started = True  # makes sure that this funktion can only be called one time 
                                       # which assures that the shipment can only be started one time

    self.shipmentStart = block.timestamp # a timestamp will be created when the function shipping_start is called 
                                         # meaning that the shipping has started 

    log.Shipment(msg.sender, self, 1)    # sends a message through the blockchain
                                         # that the shipping has startet and the contract address as well as the value 1 
                                         # which indicates that the shipping has started
    
@public
def shipping_end():
    assert msg.sender == self.oracle
    assert self.has_oem_payed == True
    assert self.has_supplier_payed == True
    assert self.has_shipment_started == True
    assert self.delivered == False

    self.delivered = True
    self.shipmentEnd = block.timestamp 

    log.Shipment(msg.sender, self, 2)

@private
def payment_from_contract():
    """
    helper function called from the quality check function that distribute
    payments based on the production and shipment duration
    """
    production_duration_total : timedelta = self.shipmentStart - self.orderStart
    shipment_duration_total : timedelta = self.shipmentEnd - self.shipmentStart

    if production_duration_total > self.production_duration_limit:

        amount_to_supplier : wei_value = self.price + self.insurance

        send(self.supplier, amount_to_supplier) # send from the contract to the supplier the price and insurance
        send(self.oem, self.balance) # sends everything left (bonus) back to the oem 

        log.Payment(self, self.supplier, amount_to_supplier) # TODO
        log.Payment(self, self.oem, self.balance)

    elif production_duration_total <= self.production_duration_limit and shipment_duration_total > self.shipment_duration_limit:

        amount_to_supplier : wei_value = self.price + self.insurance + self.bonus/2

        send(self.supplier, amount_to_supplier)
        send(self.oem, self.balance)

        log.Payment(self, self.supplier, amount_to_supplier) # TODO
        log.Payment(self, self.oem, self.balance)

    else:

        send(self.supplier, self.balance)  

        log.Payment(self, self.supplier, self.balance)

@private
def retour():
    """
    helper function called from the quality check that if quality check fails
    sends back the goods and the full contract balance (price + bonus + insurance) to the oem
    """
    
    send(self.oem, self.balance)

    log.Payment(self, self.oem, self.balance)


@public
def quality_check(passed : bool):
    # add a check if qc is already done
    assert msg.sender == self.oem
    assert self.delivered == True
    assert self.has_quality_control_being_done == False

    self.has_quality_control_being_done = True

    if passed == True:
        self.payment_from_contract()
    else:
        self.retour()

@public
def no_accepted_delivery_time():
    total_order_duration : timedelta = block.timestamp - self.orderStart # TODO

    assert msg.sender == self.oem
    assert self.delivered == False
    assert total_order_duration > self.shipment_duration_inacceptable # TODO

    send(self.oem, self.balance)