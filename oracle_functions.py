# NO FUNCTIONS

# from web3 import Web3, HTTPProvider # web3.py docs
# import os # python docs
# import json # python docs

# with open('oem_supplier_shipper.json') as json_file: 
#     data = json.load(json_file)

# abi = data['abi']

# contract_address = "0x7C1c21995af5E2920edf30D662C3fDa103423cC3"

# from_address = "0x5E536f1A1b0A21F1044244F02ABE2902576462b7"
# private_key = 'f5b35357c7ae8692c9f660585018224d3f9f92c854170ca2002a42d352a0eb33'


# w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs
# w3.eth.defaultAccount = from_address


# Contract = w3.eth.contract(abi = abi, address = contract_address)

# nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

# txn = Contract.functions.shipping_start().buildTransaction({
#     'gas' : 70000,
#     'gasPrice' : w3.toWei('1', 'gwei'),
#     'nonce' : nonce,
# })

# signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

# signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

# print(w3.eth.waitForTransactionReceipt(signed_txn_hash))

# WITH FUNCTIONS

from web3 import Web3, HTTPProvider # web3.py docs
import os # python docs
import json # python docs

with open('oem_supplier_shipper.json') as json_file: 
    data = json.load(json_file)

abi = data['abi']

contract_address = "0x7C1c21995af5E2920edf30D662C3fDa103423cC3"

from_address = "0x5E536f1A1b0A21F1044244F02ABE2902576462b7"
private_key = 'f5b35357c7ae8692c9f660585018224d3f9f92c854170ca2002a42d352a0eb33'


w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs
w3.eth.defaultAccount = from_address


Contract = w3.eth.contract(abi = abi, address = contract_address)

nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

def shipping_start(Contract = Contract, nonce = nonce, w3 = w3):

    txn = Contract.functions.shipping_start().buildTransaction({
        'gas' : 70000,
        'gasPrice' : w3.toWei('1', 'gwei'),
        'nonce' : nonce,
    })

    signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

    signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print(w3.eth.waitForTransactionReceipt(signed_txn_hash))

def shipping_end(Contract = Contract, nonce = nonce, w3 = w3):

    txn = Contract.functions.shipping_end().buildTransaction({
        'gas' : 70000,
        'gasPrice' : w3.toWei('1', 'gwei'),
        'nonce' : nonce,
    })

    signed_txn = w3.eth.account.signTransaction(txn, private_key=private_key)

    signed_txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print(w3.eth.waitForTransactionReceipt(signed_txn_hash))