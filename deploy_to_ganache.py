from web3 import Web3, HTTPProvider # web3.py docs
import os # python docs
import json # python docs

with open('oem_supplier_shipper.json') as json_file: 
    data = json.load(json_file)

abi = data['abi']

bytecode = data['bytecode']

w3 = Web3(HTTPProvider('http://localhost:7545')) # web3.py + ganache docs

Contract = w3.eth.contract(abi = abi, bytecode = bytecode) # web3.py 
                                                        # creating  an object that will be used to deploy the contract
'''
hardcoding all the values to the variables
'''
oem_address = "0x65a705ba33CeF50E52AbFE5cE833c922d989a3B8"
supplier_address = "0x7bDB60C47b0CD90179014b4e12D5cA89780bF3d6"
price = 50000000000000000000
malus = round((price * 20) / 100)
bonus = malus
insurance = bonus
production_duration_limit = 1209600 # 2 weeks
shipment_duration_limit = 604800 # 1 week
shipment_duration_inacceptable = 60 # 5 weeks 3024000

tx_hash = Contract.constructor(supplier_address, price, malus, bonus, insurance, production_duration_limit
                             , shipment_duration_limit, shipment_duration_inacceptable)

tx_hash = tx_hash.transact({'from' : oem_address}) # creating the transaction that is creating the contract on the blockchain 
                                                   # using the address from the oem, because the oem will be the one deploying the contract

tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)
print(tx_receipt)

